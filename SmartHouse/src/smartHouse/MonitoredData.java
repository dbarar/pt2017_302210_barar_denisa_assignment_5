package smartHouse;

import java.util.Calendar;

public class MonitoredData{

	private Calendar startTime;
	private Calendar endTime;
	private String labelActivity;
	private int durationMin;
	private double durationHours;
	
	public MonitoredData(Calendar startTime, Calendar endTime, String labelActivity){
		this.startTime = startTime;
		this.endTime = endTime;
		this.labelActivity = labelActivity;
		
		long seconds = Math.abs((endTime.getTimeInMillis() - startTime.getTimeInMillis())) / 1000;
		this.durationMin = (int) (seconds / 60);
		this.setDurationHours(this.durationMin / 60);
	}
	
	public String getDay(){
		return IOStream.calendarToDayCalendar(startTime);
	}

	public Calendar getStartTime() {
		return startTime;
	}

	public void setStartTime(Calendar startTime) {
		this.startTime = startTime;
	}

	public Calendar getEndTime() {
		return endTime;
	}

	public void setEndTime(Calendar endTime) {
		this.endTime = endTime;
	}

	public String getLabelActivity() {
		return labelActivity;
	}

	public void setLabelActivity(String labelActivity) {
		this.labelActivity = labelActivity;
	}
	
	public String toString(){
		return IOStream.calendarToString(startTime) + "   " + IOStream.calendarToString(endTime) + "   " + labelActivity + "     " + durationMin + "    " + durationHours + "   "; 
	}

	public int getDurationMin() {
		return durationMin;
	}

	public void setDurationMin(int durationMin) {
		this.durationMin = durationMin;
	}

	public double getDurationHours() {
		return durationHours;
	}

	public void setDurationHours(double durationHours) {
		this.durationHours = durationHours;
	}
}
