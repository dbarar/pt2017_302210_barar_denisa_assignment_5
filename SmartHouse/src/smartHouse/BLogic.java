package smartHouse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BLogic {
	private static List<MonitoredData> md = new ArrayList<MonitoredData>();
	
	public static List<MonitoredData> getMd() {
		return md;
	}

	public static void setMd(List<MonitoredData> md) {
		BLogic.md = md;
	}
	
	public static void readFile(){
		IOStream.read(md);
	}	
	
	public static void printMD(){
		for(MonitoredData m: md){
			System.out.println(m);
		}
	}
	
	/*1. Count the distinct days that appear in the monitoring data.*/
	public static int prob1(){		
		Map<String, Long> hm1 = md.stream().collect(Collectors.groupingBy(e -> IOStream.calendarToDayCalendar(e.getStartTime()), Collectors.counting()));
		return hm1.size();
	}

	/*2. Determine a map of type <String, Integer> that maps to each distinct action type the number of
	occurrences in the log. Write the resulting map into a text file.*/	
	public static Map<String, Long> prob2(){		
		Map<String, Long> hm = md
				.stream()
				.collect(
						Collectors.groupingBy(
								e -> e.getLabelActivity(), 
								Collectors.counting()));
		IOStream.write2(hm);
		return hm;		
	}
	
	/*3. Generates a data structure of type Map<Integer, Map<String, Integer>> that contains the activity
	count for each day of the log (task number 2 applied for each day of the log) and writes the result
	in a text file.*/	
	public static Map<String, Map<String, Long>> prob3(){		
		Map<String, Map<String, Long>> hm = md
				.stream()
				.collect(
						Collectors.groupingBy(
								MonitoredData::getDay,
								HashMap::new, 
								Collectors.groupingBy(
										e -> e.getLabelActivity(), 
										Collectors.counting())));
		IOStream.write3(hm);
		return hm;
	}
	
	/*4. Determine a data structure of the form Map<String, DateTime> that maps for each activity the total duration computed over the monitoring period. 
	  Filter the activities with total duration larger than 10 hours. Write the result in a text file.*/
	public static Map<String, Double> prob4(){		
		Map<String, Double> hm4 = md
				.stream()
				.collect(
					Collectors.groupingBy(
							MonitoredData::getLabelActivity,
							Collectors.summingDouble(MonitoredData::getDurationMin))
							);
		
								
		Map<String, Double> hm = (Map<String, Double>) hm4.entrySet()
									.stream()
									.filter(map -> map.getValue() > 10)
									.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));				
		IOStream.write4(hm);
		return hm;
	}
	
	/*5. Filter the activities that have 90% of the monitoring samples with duration less than 5 minutes,
	collect the results in a List<String> containing only the distinct activity names and write the result
	in a text file.*/
	public static List<String> prob5(){
		List<String> lst = null;
		
		Map<String, List<Integer>> hm = md
				.stream()
				.collect(
					Collectors.groupingBy(
								MonitoredData::getLabelActivity,
								Collectors.mapping(
										MonitoredData::getDurationMin,
										Collectors.toList())));
		lst = hm.entrySet()
				.stream()
				.filter(e -> ((e.getValue().size() * 0.9) <= (e.getValue().stream().filter(f -> f.intValue() <= 5).collect(Collectors.toList()).size())))
				.map(g -> g.getKey())
				.collect(Collectors.toList());
		IOStream.write5(lst);
		return lst;
	}
}
/*works!
 * Map<String, List<String>> hm = md
		.stream()
		.collect(
			Collectors.groupingBy(
						MonitoredData::getDay,
						Collectors.mapping(
								MonitoredData::getLabelActivity,
								Collectors.toList())));
System.out.println(hm.toString());*/