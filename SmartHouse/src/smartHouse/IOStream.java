package smartHouse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class IOStream {
	
	private static Calendar stringToCalendar(String s) throws ParseException{
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
		
		cal.setTime(sdf.parse(s));
		
		return cal;
	}
	
	public static String calendarToDayCalendar(Calendar cal){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);		
		String formatted = sdf.format(cal.getTime());
		
		return formatted;
	}
	
	public static String calendarToString(Calendar cal){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);		
		String formatted = sdf.format(cal.getTime());
		
		return formatted;
	}

	public static void read(List<MonitoredData> md){
		
		Path file = Paths.get("./Activities.txt");		
		
		Calendar startTime;
		Calendar endTime;
		String labelActivity;
		
		try (InputStream in = Files.newInputStream(file);
		    BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
		    String line = null;
		    while ((line = reader.readLine()) != null) {
		       // System.out.println(line);
		    	String delims = "[		]";
		    	String[] tokens = line.split(delims);		    	    
		
				startTime = stringToCalendar(tokens[0]);
				endTime = stringToCalendar(tokens[2]);
				labelActivity = tokens[4];				
		    	
				md.add(new MonitoredData(startTime, endTime, labelActivity));
		    }
		} catch (IOException x) {
		    System.err.println(x);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void write2(Map<String, Long> m){	
		
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Prob2.txt"), "utf-8"))) {
			for(String name: m.keySet()){
				String key = name.toString();
				String value = m.get(name).toString();

				writer.write(key + " " + value + "\n");
			}
		} catch (IOException x) {
		    System.err.println(x);
		}
	}

	public static void write3(Map<String, Map<String, Long>> m) {
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Prob3.txt"), "utf-8"))) {
			for(String name: m.keySet()){
				String key = name.toString();
				String value = m.get(name).toString();

				writer.write(key + " " + value + "\n");
			}
		} catch (IOException x) {
		    System.err.println(x);
		}
		
	}

	public static void write4(Map<String, Double> m) {
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Prob4.txt"), "utf-8"))) {
			for(String name: m.keySet()){
				String key = name.toString();
				String value = m.get(name).toString();

				writer.write(key + " " + value + "\n");
			}
		} catch (IOException x) {
		    System.err.println(x);
		}
		
	}

	public static void write5(List<String> lst) {
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Prob5.txt"), "utf-8"))) {
			writer.write(lst + "\n");
		} catch (IOException x) {
		    System.err.println(x);
		}
		
	}
}
