package presentation;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import smartHouse.BLogic;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;

public class Gui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane;

	/**
	 * Lunch the application
	 * @param args
	 */
	public static void main(String[] args) {
		
		BLogic.readFile();
		//BLogic.printMD();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Gui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 614, 352);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 228, 196));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(10, 58, 364, 221);
		contentPane.add(textPane);
		
		JButton button = new JButton("1");
		button.setBounds(392, 40, 89, 23);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textPane.setText("Count the distinct days that appear in the monitoring data.");
				System.out.println(BLogic.prob1());
			}
		});
		contentPane.add(button);
		
		JButton button_1 = new JButton("2");
		button_1.setBounds(392, 95, 89, 23);
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textPane.setText("Determine a map of type <String, Integer> that maps to each distinct action type the number of occurrences in the log. Write the resulting map into a text file.");
				BLogic.prob2();
			}
		});
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("3");
		button_2.setBounds(392, 141, 89, 23);
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textPane.setText("Generates a data structure of type Map<Integer, Map<String, Integer>> that contains the activity	count for each day of the log (task number 2 applied for each day of the log) and writes the result in a text file.");
				BLogic.prob3();
			}
		});
		contentPane.add(button_2);
		
		JButton button_3 = new JButton("4");
		button_3.setBounds(392, 186, 89, 23);
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textPane.setText("Determine a data structure of the form Map<String, DateTime> that maps for each activity the total duration computed over the monitoring period. Filter the activities with total duration larger than 10 hours. Write the result in a text file.");
				BLogic.prob4();
			}
		});
		contentPane.add(button_3);
		
		JButton button_4 = new JButton("5");
		button_4.setBounds(392, 230, 89, 23);
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textPane.setText("Filter the activities that have 90% of the monitoring samples with duration less than 5 minutes, collect the results in a List<String> containing only the distinct activity names and write the result in a text file.");
				BLogic.prob5();
			}
		});
		contentPane.add(button_4);
		
		
		
		
	}
}
